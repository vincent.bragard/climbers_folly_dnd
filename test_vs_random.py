from climbers_folly import *
import sys

if __name__ == '__main__':
    nb_players = 2

    state = ClimbersFollyState(nb_players)

    iters = 2000

    is_random_vs_random = False

    anchor_chance_player_1 = 100
    anchor_chance_player_2 = 100

    if len(sys.argv) == 2:
        # One argument in case we want to change the number of iterations
        iters = int(sys.argv[1])
    elif len(sys.argv) == 3:
        # Two arguments for random vs random
        is_random_vs_random = True
        anchor_chance_player_1 = int(sys.argv[1])
        anchor_chance_player_2 = int(sys.argv[2])

    while state.get_moves():
        print("{}".format(state))
        if state.player_turn == 0:
            if not is_random_vs_random:
                m = uct(root_state=state, iter_max=iters, verbose=False)
            else:
                m = state.get_random_move(anchor_chance_player_1)
        else:
            m = state.get_random_move(anchor_chance_player_2)
        print("Player {} best move: {}\n".format(state.player_turn + 1, m))
        state.do_move(m)

    print("Player {} wins!".format(state.player_winner + 1))

# Results of comparing different random strategies (varying the anchoring chance), matches of 1600 games
#       100     90      80      70      60      50      40      30      20      10      0
# 100           51.4    54.2    54.2    55.8    55.8    58.3    62.9    67.8    68.6    75.8
# 90                    51.3    54.4    52.9    56.6    57.5    63.1    62.3    69.7    77.3
# 80                            52.1    54.4    54.4    58.4    61.5    65.4    70.8    75.8
# 70                                    53.7    53.8    56.1    58.9    63.9    69.0    76.6
# 60                                            51.8    53.3    58.1    59.2    67.9    72.8
# 50                                                    53.2    57.6    63.1    65.3    72.3
# 40                                                            53.3    57.6    65.3    71.0
# 30                                                                    57.3    60.7    70.4
# 20                                                                            55.6    65.9
# 10                                                                                    64.4

# Best random strategy: always anchoring if possible (100% anchor chance)
# MCTS VS "Always anchor" winrate (1600 games, sdev 1.25%):
#    500 iterations: 872 wins, 728 losses --> 54.5% winrate
#    1000 iterations: 888 wins, 712 losses --> 55.5% winrate
#    2000 iterations: 914 wins, 686 losses --> 57.125% winrate
#    4000 iterations: 910 wins, 690 losses --> 56.875% winrate
# This indicates that more than 2000 iterations might not be needed. At this point the randomness is too strong compared
# to any advantage more simulations might provide.
