import copy
import random
import json
from math import *


class ClimbersFollyState:
    def __init__(self, nb_players=2):
        self.players = []
        self.player_just_moved = nb_players - 1
        self.player_turn = 0
        self.player_winner = -1

        # Each player starts with a dice size of 4 and an anchor of 0
        # The "current" will be used to store current climbing height since last anchor
        for i in range(nb_players):
            cur_player = {'anchor_size': 6, 'anchor_val': 0, 'current_size': 6, 'current_val': 0}
            self.players.append(cur_player)

        # We initialize all players so the game can be started right away
        max_val = 0
        max_idx = -1

        for idx, player in enumerate(self.players):
            # Rolling the inital d4 for each player then choosing the first player as the one with the highest starting
            # anchor
            player['anchor_val'] = player['current_val'] = random.randint(1, 4)

            if player['anchor_val'] > max_val:
                max_val = player['anchor_val']
                max_idx = idx - 1

        self.player_just_moved = max_idx % nb_players
        self.player_turn = (max_idx + 1) % nb_players

    def clone(self):
        """ Creates a deep clone of the game state """
        st = ClimbersFollyState()
        st.players = copy.deepcopy(self.players)
        st.player_just_moved = self.player_just_moved
        st.player_winner = self.player_winner
        st.player_turn = self.player_turn
        return st

    def do_move(self, val):
        if val > self.players[self.player_turn]['current_val']:
            # If the val is better than the current val, we go up in size
            self.players[self.player_turn]['current_val'] = val
            self.players[self.player_turn]['current_size'] += 2
            self.player_just_moved = self.player_turn

            if self.players[self.player_turn]['current_size'] > 12:
                self.player_winner = self.player_turn
        elif val > 0:
            # If the val is inferior or equal instead, we go back to last anchor and it becomes the next player's turn
            self.players[self.player_turn]['current_val'] = self.players[self.player_turn]['anchor_val']
            self.players[self.player_turn]['current_size'] = self.players[self.player_turn]['anchor_size']
            self.player_just_moved = self.player_turn
            self.player_turn = (self.player_turn + 1) % len(self.players)
        else:
            # A move of "0" means anchoring
            self.players[self.player_turn]['anchor_val'] = self.players[self.player_turn]['current_val']
            self.players[self.player_turn]['anchor_size'] = self.players[self.player_turn]['current_size']
            self.player_just_moved = self.player_turn
            self.player_turn = (self.player_turn + 1) % len(self.players)

    def get_moves(self):
        # Returns a list of all available moves
        if self.player_winner < 0:
            current_size = self.players[self.player_turn]['current_size']
            if current_size == self.players[self.player_turn]['anchor_size']:
                # You have to roll at least once on your turn before having the choice of anchoring
                return list(range(1, current_size + 1))
            else:
                # If you have rolled once on your turn, you can anchor yourself
                return list(range(0, current_size + 1))
        else:
            # If there is already a winner, the game has ended, no more moves returned
            return []

    def get_random_move(self, anchor_percent_chance=100):
        # Returns a random move among the possible ones (used in mcts rollouts to gain speed)
        if self.player_winner < 0:
            current_size = self.players[self.player_turn]['current_size']
            if current_size == self.players[self.player_turn]['anchor_size']:
                return random.randint(1, current_size)
            else:
                if anchor_percent_chance >= 100:
                    # Anchoring all the time (by default) when it is possible
                    # This is the best "dumb" strategy found after having run a lot of games between different
                    # fast strategies
                    return 0
                elif anchor_percent_chance <= 0:
                    return random.randint(1, current_size)
                elif random.randint(1, 100) <= anchor_percent_chance:
                    return 0
                else:
                    return random.randint(1, current_size)
        else:
            return None

    def get_result(self, player):
        # Returns if the given player is the winner or not
        return self.player_winner == player

    def __repr__(self):
        # Function called when printing the object
        s = ""

        for idx, player in enumerate(self.players):
            s += "Player {}: {}\n".format(idx + 1, json.dumps(player))

        s += "Winner: {}".format(self.player_winner)

        return s


class Node:
    def __init__(self, move=None, parent=None, state=None):
        self.move = move
        self.parent_node = parent
        self.child_nodes = []
        self.wins = 0
        self.visits = 0
        self.untried_moves = state.get_moves()
        self.player_just_moved = state.player_just_moved
        self.player_turn = state.player_turn

    def uct_select_child(self):
        # We select the best child with the UCT heuristic, but instead of choosing between each possible value, we
        # congregate all the possible values from the die and compare the two decisions "anchor" and "roll" instead
        decisions = [{'wins': 0, 'visits': 0, 'nodes': []}, {'wins': 0, 'visits': 0, 'nodes': []}]

        for cur_node in self.child_nodes:
            if cur_node.move == 0:
                decisions[0]['wins'] += cur_node.wins
                decisions[0]['visits'] += cur_node.visits
                decisions[0]['nodes'].append(cur_node)
            else:
                decisions[1]['wins'] += cur_node.wins
                decisions[1]['visits'] += cur_node.visits
                decisions[1]['nodes'].append(cur_node)

        if len(decisions[0]['nodes']) == 0:
            return random.choice(decisions[1]['nodes'])
        elif len(decisions[1]['nodes']) == 0:
            return decisions[0]['nodes'][0]
        else:
            uct_value_anchor = decisions[0]['wins'] / decisions[0]['visits'] + sqrt(2*log(self.visits) / decisions[0]['visits'])
            uct_value_roll = decisions[1]['wins'] / decisions[1]['visits'] + sqrt(2*log(self.visits) / decisions[1]['visits'])

            if uct_value_anchor < uct_value_roll:
                return random.choice(decisions[1]['nodes'])
            else:
                return decisions[0]['nodes'][0]

    def add_child(self, move, state):
        # Function used in the expansion of the tree in the UCT algorithm
        node = Node(move=move, parent=self, state=state)
        self.untried_moves.remove(move)
        self.child_nodes.append(node)
        return node

    def update(self, result):
        # Function used in the backpropagation of the UCT algorithm
        # "visits" contain the number of simulations which passed through this node
        # "wins" contains the number of simulations which ended in a win from the perspective of the player_just_moved
        self.visits += 1
        self.wins += result

    def __repr__(self):
        return ""


def uct(root_state, iter_max, verbose=False):
    root_node = Node(state=root_state)

    for i in range(iter_max):
        node = root_node
        state = root_state.clone()

        # Select: Choosing which node to explore based on a heuristic
        # This tries to focus computing on "promising" branches of the tree
        while node.untried_moves == [] and node.child_nodes != []:
            node = node.uct_select_child()
            state.do_move(node.move)

        # Expand: Expands the tree by inserting one more node at the end of the current branch
        if node.untried_moves:
            move = random.choice(node.untried_moves)
            state.do_move(move)
            node = node.add_child(move, state)

        # Rollout: Makes a fast simulation starting from the end of the branch all the way to an end-game position
        terminal = False
        while not terminal:
            move = state.get_random_move()

            if move is None:
                terminal = True
            else:
                state.do_move(move)

        # print("Terminal node reached: {}".format(state))

        # Backpropagate: Updates each node's "visit" and "wins" in the current branch with the result of the simulation
        while node is not None:
            node.update(state.get_result(node.player_just_moved))
            node = node.parent_node

    # All the simulations done result in an uneven number of visits on each node of the root: the better a move
    # according to the simulations, the more visits its node will have received.
    # Here, we aggregate the visits and wins of every dice roll becaus ein reality we can't choose what we roll,
    # so we choose in the end between "rolling" and "anchoring"
    decisions = [{'wins': 0, 'visits': 0, 'nodes': []}, {'wins': 0, 'visits': 0, 'nodes': []}]
    values = {}

    for cur_node in root_node.child_nodes:
        if cur_node.move == 0:
            decisions[0]['wins'] += cur_node.wins
            decisions[0]['visits'] += cur_node.visits
            decisions[0]['nodes'].append(cur_node)
        else:
            decisions[1]['wins'] += cur_node.wins
            decisions[1]['visits'] += cur_node.visits
            decisions[1]['nodes'].append(cur_node)

        values[cur_node.move] = {}
        values[cur_node.move]['wins'] = cur_node.wins
        values[cur_node.move]['visits'] = cur_node.visits

    light_decisions = [{'wins': decisions[0]['wins'], 'visits': decisions[0]['visits']}, {'wins': decisions[1]['wins'], 'visits': decisions[1]['visits']}]
    print("{}\n{}".format(json.dumps(values), json.dumps(light_decisions)))

    if len(decisions[0]['nodes']) == 0:
        return random.choice(decisions[1]['nodes']).move
    elif len(decisions[1]['nodes']) == 0:
        return 0
    else:
        if (decisions[0]['wins'] / decisions[0]['visits']) < \
                (decisions[1]['wins'] / decisions[1]['visits']):
            # If we decide to roll, we generate a random integer to simulate the dice roll and we return it.
            return random.choice(decisions[1]['nodes']).move
        else:
            return 0


def uct_play_game():
    nb_players = 4

    state = ClimbersFollyState(nb_players)

    while state.get_moves():
        print("{}".format(state))
        m = uct(root_state=state, iter_max=10000, verbose=False)
        print("Player {} best move: {}\n".format(state.player_turn + 1, m))
        state.do_move(m)

    print("Player {} wins!".format(state.player_winner + 1))


if __name__ == '__main__':
    uct_play_game()
